<?php

namespace Tests\Api;

use Illuminate\Http\Response;
use Tests\TestCase;

class HealthTest extends TestCase
{
    /**
     * @test
     */
    public function health()
    {
        $this->get(route('api::health-check'))
            ->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
