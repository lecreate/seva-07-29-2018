<?php

namespace Tests\Repositories;

use App\Models\File;
use App\Repositories\Files\Contracts\FileRepositoryInterface;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Mockery;
use PDOException;
use Psr\Log\LoggerInterface;
use Storage;
use Tests\TestCase;

class FileControllerTest extends TestCase
{
    /**
     * @var FileRepositoryInterface|Mockery
     */
    private $repo;

    /**
     * @var Filesystem|Mockery
     */
    private $storage;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $endpoints;

    public function setUp()
    {
        parent::setUp();

        $this->repo = Mockery::mock(FileRepositoryInterface::class);
        $this->app->instance(FileRepositoryInterface::class, $this->repo);

        $this->storage = Mockery::mock(Filesystem::class);
        $this->app->instance(Filesystem::class, $this->storage);

        $this->logger = Mockery::mock(LoggerInterface::class);
        $this->app->instance(LoggerInterface::class, $this->logger);

        $this->endpoints = [
            'get' => route('api::v1::files::get'),
            'post' => route('api::v1::files::post'),
            'delete' => route('api::v1::files::delete', ['uuid' => 'test'])
        ];
    }

    /**
     * @test
     */
    public function getFilesReturnOk()
    {
        $testCount = 5;

        $factories = factory(File::class, $testCount)->make();

        $this->repo
            ->shouldReceive('getAll')
            ->once()
            ->andReturn($factories);

        $this
            ->json('get', $this->endpoints['get'])
            ->assertStatus(200)
            ->assertJsonCount(5)
            ->assertJsonStructure([
                '*' => [
                    'uuid',
                    'name',
                    'type'
                ]
            ]);
    }

    /**
     * @test
     */
    public function getFilesReturnServerError()
    {
        $this->repo
            ->shouldReceive('getAll')
            ->once()
            ->andThrow(PDOException::class);

        $this->logger
            ->shouldReceive('error')
            ->once()
            ->andReturnSelf();

        $this
            ->json('get', $this->endpoints['get'])
            ->assertStatus(500)
            ->assertJsonStructure(['error']);
    }

    /**
     * @test
     */
    public function postFileReturnOk()
    {
        $factory = factory(File::class)->make();

        $this->repo
            ->shouldReceive('create')
            ->once()
            ->andReturn($factory);

        $response = $this->json('post', $this->endpoints['post'], [
            'file' => UploadedFile::fake()->create('test.json', 100)
        ]);

        $response->assertStatus(201);

        $uploadedFile = config('file-upload.upload-path') . '/' . $factory->uuid;

        $this->assertTrue(Storage::exists($uploadedFile));
        Storage::delete($uploadedFile);
    }

    /**
     * @test
     */
    public function postFileInvalidType()
    {
        $response = $this->json('post', $this->endpoints['post'], [
            'file' => UploadedFile::fake()->create('test.txt', 100)
        ]);

        $response
            ->assertStatus(400)
            ->assertJsonStructure(['error']);
    }

    /**
     * @test
     */
    public function postFileInvalidSize()
    {
        $this->logger
            ->shouldReceive('error')
            ->once()
            ->andReturnSelf();

        $response = $this->json('post', $this->endpoints['post'], [
            'file' => UploadedFile::fake()->create('test.txt', 1025)
        ]);

        $response
            ->assertStatus(413)
            ->assertJsonStructure(['error']);
    }

    /**
     * @test
     */
    public function deleteFileReturnOk()
    {
        $this->repo
            ->shouldReceive('deleteByUuid')
            ->once()
            ->with('test')
            ->andReturn(true);

        $this->storage
            ->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        $this->logger
            ->shouldReceive('info')
            ->once()
            ->andReturnSelf();

        $this->json('delete', $this->endpoints['delete'])
            ->assertStatus(204);
    }

    /**
     * @test
     */
    public function deleteFileReturnNotFound()
    {
        $this->repo
            ->shouldReceive('deleteByUuid')
            ->once()
            ->with('test')
            ->andReturn(false);

        $this->logger
            ->shouldReceive('error')
            ->once()
            ->andReturnSelf();

        $this->json('delete', $this->endpoints['delete'])
            ->assertStatus(404)
            ->assertJsonStructure(['error']);
    }
}
