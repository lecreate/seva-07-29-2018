<?php

namespace Tests\Repositories;

use App\Models\File;
use App\Repositories\Files\Eloquent\FileRepository;
use Mockery;
use PDOException;
use Tests\TestCase;

class FileRepositoryTest extends TestCase
{
    /**
     * @var Mockery\MockInterface
     */
    private $model;

    /**
     * @var FileRepository
     */
    private $repo;

    /**
     * FileRepositoryTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->model = Mockery::mock(File::class);
        $this->model
            ->shouldReceive('getKeyName')
            ->andReturn('id');

        $this->repo = new FileRepository($this->model);
    }

    /**
     * @test
     */
    public function getAllReturnsCollection()
    {
        $testCount = 5;

        $factories = factory(File::class, $testCount)->make();

        $this->model
            ->shouldReceive('orderBy')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('get')
            ->once()
            ->andReturn($factories);

        $response = $this->repo
            ->getAll();

        $this->assertNotEmpty($response);
        $this->assertEquals($testCount, $response->count());
    }

    /**
     * @test
     * @expectedException PDOException
     */
    public function getAllThrowsPdoException()
    {
        $this->model
            ->shouldReceive('orderBy')
            ->once()
            ->andThrow(PDOException::class);

        $this->repo->getAll();
    }

    /**
     * @test
     */
    public function deleteByUuidReturnTrue()
    {
        $testUuid = 'test';

        $factory = factory(File::class)->make();

        $this->model
            ->shouldReceive('where')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('first')
            ->once()
            ->andReturn($factory);

        $response = $this->repo
            ->deleteByUuid($testUuid);
        $this->assertTrue($response);
    }

    /**
     * @test
     */
    public function deleteByUuidReturnFalse()
    {
        $testUuid = 'test';

        $this->model
            ->shouldReceive('where')
            ->once()
            ->andReturnSelf()
            ->shouldReceive('first')
            ->once()
            ->andReturn(null);

        $response = $this->repo
            ->deleteByUuid($testUuid);
        $this->assertFalse($response);
    }

    /**
     * @test
     * @expectedException PDOException
     */
    public function deleteByUuidThrowPdoException()
    {
        $testUuid = 'test';

        $this->model
            ->shouldReceive('where')
            ->once()
            ->andThrow(PDOException::class);

        $this->repo->deleteByUuid($testUuid);
    }
}
