<?php

namespace App\Repositories\Files\Eloquent;

use App\Repositories\Files\Contracts\FileRepositoryInterface;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use PDOException;

/**
 * Class FileRepository.
 *
 * @package App\Repositories\Files\Eloquent
 */
class FileRepository extends BaseRepository implements FileRepositoryInterface
{
    /**
     * @throws PDOException
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->all();
    }

    /**
     * @param string $uuid
     * @throws PDOException
     * @return bool
     */
    public function deleteByUuid(string $uuid): bool
    {
        try {
            $model = $this->firstBy([
                'uuid' => $uuid
            ]);
        } catch (ModelNotFoundException $e) {
            return false;
        }

        $model->delete();

        return true;
    }
}
