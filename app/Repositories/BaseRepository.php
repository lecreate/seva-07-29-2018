<?php

namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use PDOException;

/**
 * Class BaseRepository.
 *
 * @package App\Repositories
 */
class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var Model|Builder|QueryBuilder|Collection
     */
    protected $model;

    /**
     * @param Model|\Mockery\MockInterface $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @throws PDOException
     * @return Model
     */
    public function create(array $data): Model
    {
        try {
            $model = $this->model->create($data);
        } catch (Exception $e) {
            throw new PDOException($e->getMessage());
        }

        return $model;
    }

    /**
     * @throws PDOException
     * @return Collection
     */
    public function all(): Collection
    {
        try {
            $data = $this->model->orderBy($this->model->getKeyName(), 'DESC')->get();
        } catch (Exception $e) {
            throw new PDOException($e->getMessage());
        }

        return $data;
    }

    /**
     * @param array $clauses
     * @throws ModelNotFoundException
     * @throws PDOException
     * @return Model
     */
    public function firstBy(array $clauses): Model
    {
        try {
            $model = $this->model;

            foreach ($clauses as $field => $value) {
                $model = $model->where($field, $value);
            }

            $model = $model->first();
        } catch (Exception $e) {
            throw new PDOException($e->getMessage());
        }

        if (empty($model)) {
            throw new ModelNotFoundException();
        }

        return $model;
    }
}
