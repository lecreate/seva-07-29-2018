<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use PDOException;

/**
 * Interface BaseRepositoryInterface.
 *
 * @package App\Repositories
 */
interface BaseRepositoryInterface
{
    /**
     * Creates a new entry into the database and returns the new model to the user.
     *
     * @param array $data
     * @throws PDOException
     * @return Model
     */
    public function create(array $data): Model;

    /**
     * Returns all models from the database that match the requested resources.
     *
     * @throws PDOException
     * @return Collection
     */
    public function all(): Collection;

    /**
     * Returns single model from the database that match the passed clauses.
     *
     * @param array $clauses
     * @throws ModelNotFoundException
     * @throws PDOException
     * @return Model
     */
    public function firstBy(array $clauses): Model;
}
