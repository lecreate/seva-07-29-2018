<?php

namespace App\Processors;

use App\Repositories\BaseRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Interface BaseProcessorInterface.
 *
 * @package App\Processors
 */
interface BaseProcessorInterface
{
    /**
     * Sets the laravel request for the processor instance.
     *
     * @param Request $request
     * @return BaseProcessorInterface
     */
    public function setRequest(Request $request): BaseProcessorInterface;

    /**
     * Sets the request entity for the processor interface.
     *
     * @param string $entity
     * @return BaseProcessorInterface
     */
    public function setRequestEntity(string $entity): BaseProcessorInterface;

    /**
     * Sets the response entity for the processor interface.
     *
     * @param string $entity
     * @return BaseProcessorInterface
     */
    public function setResponseEntity(string $entity): BaseProcessorInterface;

    /**
     * Sets the repository for the processor instance.
     *
     * @param BaseRepositoryInterface $repository
     * @return BaseProcessorInterface
     */
    public function setRepository(BaseRepositoryInterface $repository): BaseProcessorInterface;

    /**
     * Kicks off the processor and triggers the application to start.
     *
     * @return Response
     */
    public function dispatch(): Response;
}
