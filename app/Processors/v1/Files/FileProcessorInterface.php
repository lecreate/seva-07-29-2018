<?php

namespace App\Processors\v1\Files;

use App\Processors\BaseProcessorInterface;

/**
 * Interface FileProcessorInterface.
 *
 * @package App\Processors\v1\Files
 */
interface FileProcessorInterface extends BaseProcessorInterface
{
}
