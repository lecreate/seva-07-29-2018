<?php

namespace App\Providers;

use DateTime;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

/**
 * Class SerializerServiceProvider.
 *
 * @package App\Providers
 */
class SerializerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind(SerializerInterface::class, function (Application $app) {
            $serializer = SerializerBuilder::create()
                ->setDebug(config('app.debug'))
                ->configureHandlers(function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(
                        new DateHandler(
                            DateTime::ISO8601,
                            date_default_timezone_get(),
                            true
                        )
                    );
                })
                ->addMetadataDirs([
                    'App\Entities\v1\Files' => base_path('app/Entities/v1/Files/Metadata'),
                ]);

            if ($app->environment() !== 'testing') {
                $serializer->setCacheDir(storage_path());
            }

            return $serializer->build();
        });
    }
}
