<?php

namespace App\Observers;

use App\Models\File;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class FileObserver
{
    /**
     * Handle the file "creating" event.
     *
     * @param  \App\Models\File  $file
     * @return void
     *
     * @throws UnsatisfiedDependencyException
     */
    public function creating(File $file)
    {
        // TODO: move uuid creation into service
        $file->uuid = Uuid::uuid1();
    }
}
