<?php

namespace App\Entities\v1\Files;

use App\Entities\BaseEntity;
use App\Entities\EntityInterface;
use Illuminate\Validation\Validator;

/**
 * Class GetFilesResponse.
 *
 * @package App\Entities\v1\Files
 */
class GetFilesResponse extends BaseEntity implements EntityInterface
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $size;

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $value
     * @return GetFilesResponse
     */
    public function setUuid(string $value): GetFilesResponse
    {
        $this->uuid = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $value
     * @return GetFilesResponse
     */
    public function setName(string $value): GetFilesResponse
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $value
     * @return GetFilesResponse
     */
    public function setType(string $value): GetFilesResponse
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $value
     * @return GetFilesResponse
     */
    public function setSize(int $value): GetFilesResponse
    {
        $this->size = $value;

        return $this;
    }
}
