<?php

namespace App\Entities\v1\Files;

use App\Entities\BaseEntity;
use App\Entities\EntityInterface;
use Illuminate\Validation\Validator;

/**
 * Class PostFileRequest.
 *
 * @package App\Entities\v1\Files
 */
class PostFileRequest extends BaseEntity implements EntityInterface
{
    /**
     * @var \Illuminate\Http\UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $size;

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $value
     * @return PostFileRequest
     */
    public function setFile($value): PostFileRequest
    {
        $this->file = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $value
     * @return PostFileRequest
     */
    public function setName(string $value): PostFileRequest
    {
        $this->name = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $value
     * @return PostFileRequest
     */
    public function setType(string $value): PostFileRequest
    {
        $this->type = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $value
     * @return PostFileRequest
     */
    public function setSize(int $value): PostFileRequest
    {
        $this->size = $value;

        return $this;
    }

    /**
     * @return Validator
     */
    public function validate(): Validator
    {
        $data = [
            'name' => $this->name,
            'size' => $this->size,
            'type' => $this->type
        ];

        $rules = [
            'name' => 'required|string',
            'size' => 'required|integer',
            'type' => 'required|string|in:' . config('file-upload.types-accepted')
        ];

        $validator = $this->getValidator($data, $rules);

        return $validator;
    }

    /**
     * @return Validator
     */
    public function validateFile(): Validator
    {
        $data = [
            'file' => $this->file
        ];

        $rules = [
            'file' => 'required|file'
        ];

        $validator = $this->getValidator($data, $rules);

        return $validator;
    }

    /**
     * @return Validator
     */
    public function validateFileSize(): Validator
    {
        $data = [
            'file' => $this->file
        ];

        $rules = [
            'file' => 'max:' . config('file-upload.max-size')
        ];

        $validator = $this->getValidator($data, $rules);

        return $validator;
    }
}
