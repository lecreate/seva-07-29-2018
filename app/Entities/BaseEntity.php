<?php

namespace App\Entities;

use JMS\Serializer\SerializerInterface;
use TypeError;
use Validator;

/**
 * Class BaseEntity.
 *
 * @package App\Entities
 */
class BaseEntity implements EntityInterface
{
    /**
     * Returns serialized json object.
     *
     * @return string
     */
    public function __toString()
    {
        return app(SerializerInterface::class)->serialize($this, 'json');
    }

    /**
     * De-Serializes the json string into it's entity representation.
     *
     * @param string|array|EntityInterface $data
     *
     * @return EntityInterface
     */
    public function deserialize($data): EntityInterface
    {
        if ($data instanceof EntityInterface) {
            $data = $data->toArray();
        }

        if (is_array($data)) {
            $data = json_encode($data);
        }

        return app(SerializerInterface::class)->deserialize($data, get_class($this), 'json');
    }

    /**
     * Returns a json string with only the set fields.
     *
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * Creates An Array From The Entity.
     *
     * @param null $nested
     *
     * @return array
     */
    public function toArray($nested = null): array
    {
        $methods = get_class_methods($nested) ?? get_class_methods($this);
        $methods = array_filter($methods, function ($method) {
            return (strpos($method, 'get') !== false) || (strpos($method, 'is') !== false);
        });

        foreach ($methods as $method) {
            $label = (strpos($method, 'get') === false)
                ? lcfirst(substr($method, 2, strlen($method)))
                : lcfirst(substr($method, 3, strlen($method)));

            try {
                $result = $nested === null ? $this->$method() : $nested->$method();
            } catch (TypeError $e) {
                continue;
            }

            if ($result instanceof EntityInterface) {
                $structure[$label] = $this->toArray($result);
                continue;
            }

            if (is_array($result)) {
                foreach ($result as $value) {
                    $structure[$label] = $this->toArray($value);
                }

                continue;
            }

            $structure[$label] = $result;
        }

        return $structure ?? [];
    }

    /**
     * Validates request.
     *
     * @param array $data
     * @param array $rules
     *
     * @return \Illuminate\Validation\Validator
     */
    public function getValidator(array $data, array $rules): \Illuminate\Validation\Validator
    {
        return Validator::make($data, $rules);
    }
}
