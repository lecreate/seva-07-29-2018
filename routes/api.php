<?php

use Illuminate\Http\Response;

Route::group(['as' => 'api::'], function() {

    Route::get('/', ['as' => 'health-check', 'uses' => function () {
        return response(null, Response::HTTP_NO_CONTENT);
    }]);

    Route::group(['prefix' => 'v1', 'as' => 'v1::'], function() {
        Route::group(['prefix' => 'files', 'as' => 'files::'], function() {
            Route::get('', ['as' => 'get', 'uses' => v1\FileController::class . '@get']);
            Route::post('', ['as' => 'post', 'uses' => v1\FileController::class . '@post']);
            Route::delete('{uuid}', ['as' => 'delete', 'uses' => v1\FileController::class . '@delete']);
        });
    });

});