<?php

return [
    'types-accepted'    => env('FILE_UPLOAD_TYPES_ENABLED', ''),
    'max-size'          => env('FILE_UPLOAD_MAX_SIZE', 2048),
    'upload-path'       => env('FILE_UPLOAD_PATH','files')
];