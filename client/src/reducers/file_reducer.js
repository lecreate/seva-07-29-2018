import {
    FETCH_FILE, FETCH_FILE_SUCCESS, ADD_FILE, ADD_FILE_SUCCESS, ADD_FILE_ERROR, DELETE_FILE, DELETE_FILE_SUCCESS, DELETE_FILE_ERROR
} from '../actions/types';

const INITIAL_STATE = {
    filesList: {files: [], error: null, loading: false},
    newFile: {file: null, error: null, loading: false},
    deletedFile: {file: null, error: null, loading: false}
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_FILE:
            return {
                ...state,
                filesList: {files: [], error: null, loading: true},
                deletedFile: {file: null, error: null, loading: false}
            };
        case FETCH_FILE_SUCCESS:
            return {
                ...state,
                filesList: {files: action.payload.data, error: null, loading: false},
                newFile: {file: null, error: null, loading: false}
            };
        case ADD_FILE:
            return {...state, newFile: {file: null, error: null, loading: true}};
        case ADD_FILE_SUCCESS:
            return {...state, newFile: {file: "created", error: null, loading: false}};
        case ADD_FILE_ERROR:
            return {...state, newFile: {file: null, error: action.payload, loading: false}};
        case DELETE_FILE:
            return {...state, deletedFile: {file: null, error: null, loading: true}};
        case DELETE_FILE_SUCCESS:
            return {...state, deletedFile: {file: "deleted", error: null, loading: false}};
        case DELETE_FILE_ERROR:
            return {...state, deletedFile: {file: null, error: null, loading: false}};
        default:
            return state;
    }
}

