import React,{Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';

class Header extends Component {

    renderLinks() {
        return[
            <li className="nav-item pull-xs-left" key={2}>
                <Link className="nav-item nav-link" to="/file/add">New File</Link>
            </li>,
        ];
    }

    render() {
        return (
            <nav className="navbar navbar-light bg-faded">
                <Link to="/" className="navbar-brand">Files</Link>
                <ul className="nav navbar-nav">
                    {this.renderLinks()}
                </ul>
            </nav>
        )
    }
}

function mapStateToProps(state){
   return {

   };
}

export default connect(mapStateToProps)(Header)