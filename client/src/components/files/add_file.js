import React, {Component} from 'react';
import {addFile} from  '../../actions/index';
import {reduxForm} from 'redux-form';

class AddFile extends Component {
    static contextTypes = {
        router: React.PropTypes.object
    };

    constructor(props) {
        super(props);
        this.state = {
            file: null
        };
        this.onFileChange = this.onFileChange.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.newFile.file === "created") {
            this.context.router.push('/files');
        }
    }

    handleFormSubmit(formProps){
        this.props.addFile(this.state.file);
    }

    onFileChange(e) {
        this.setState({file: e.target.files[0]})
    }

    render() {
        const {handleSubmit, fields:{file}} = this.props;
        const {loading, error} = this.props.newFile;

        let errorMessage = 'Error happened';
        if (error !== null) {
            if (error.data.error) {
                errorMessage = error.data.error;
            }
            if (error.status === 413) {
                errorMessage = 'File size is too large';
            }
        }

        if (loading === true) {
            return <div className="loader"></div>;
        }
        return (
            <div className="row">
                <div className="col-md-12">
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                        <fieldset className="form-group">
                            <label>File:</label>
                            <input type="file" className="form-control" onChange={this.onFileChange} />
                            {error !== null && <div className="text-danger">{errorMessage}</div>}
                        </fieldset>
                        <button className="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
        );
    }
}

function validate(formProps){
    const errors = {};

    if (!formProps.file){
        errors.file = "File is required";
    }

    return errors;
}

function mapStateToProps(state){
    return {
        files: state.file,
        newFile: state.files.newFile
    }
}

export default reduxForm({
    form: 'file',
    fields: ['file'],
    //validate: validate, // TODO fix this otherwise form submits with no file
}, mapStateToProps, {addFile})(AddFile);
