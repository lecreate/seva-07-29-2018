import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {Router,Route,IndexRoute,browserHistory} from 'react-router';
import thunk from 'redux-thunk';

import App from './components/app';
import Welcome from './components/welcome';
import Files from './components/files/files';
import addFile from './components/files/add_file';

import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducers);

ReactDOM.render(
  <Provider store={store}>
    <Router history ={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Welcome} />
        <Route path="files" component={Files} />
        <Route path="file/add" component={addFile} />
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.container'));