# Kraken coding challenge

## *Disclaimer
Hello there, you are looking at the app that I was able to put together during 48hr
Kraken.com coding challenge. I spent half time working on the api side 
using fresh install of most recent Laravel and implemented 3 api endpoints
to serve the front end app. 

I left a few TODO's there and was plan on improving things if I'll have any
time left.. if it's still there means I ran out of time. 

Reactjs app is located in `client` folder. This is my first and only experience
using reactjs and I'm not any proud of what I built here (I suspect some things shouldn't be done in the way 
I did), but this is what I was able to put together using an existing boilerplate 
([from here](https://github.com/onerciller/react-redux-laravel)) in one day. 
Wasn't able to implement UI filters, cover all of the edge and error cases and write unit tests
due to lack of time.

Let me know if you have any questions. 

## Requirements
```
NPM
Composer
Docker
```

### Installation
```
composer install
docker-compose exec app php artisan migrate

cd client
npm install
```

## Api Documentation
[Api Documentation](/documentation/swagger.json) (using [swagger](https://swagger.io))


### Api Configuration
```
#FILE_UPLOAD_TYPES_ENABLED="pdf,doc"
#FILE_UPLOAD_MAX_SIZE=2048
#FILE_UPLOAD_PATH=files
```

### Run Api
```
docker-compose up -d
```

### Run App
```
cd client
npm start
```

#### Testing
```
./vendor/bin/phpunit
```

#### *Notes
- nginx and php file upload settings need to be configured otherwise 413 
status code getting returned without cors headers before request reaches the api