FROM php:7.2-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev libreadline-dev \
    mysql-client libmagickwand-dev --no-install-recommends \
    && docker-php-ext-install pdo_mysql

ARG INSTALL_XDEBUG=true
ARG INSTALL_MCRYPT=true

#####################################
# mcrypt:
#####################################

RUN if [ ${INSTALL_MCRYPT} = true ]; then \
    # Install the mcrypt extension
    pecl install mcrypt-1.0.1 && \
    docker-php-ext-enable mcrypt \
;fi

#####################################
# xDebug:
#####################################

RUN if [ ${INSTALL_XDEBUG} = true ]; then \
    # Install the xdebug extension
    pecl install xdebug && \
    docker-php-ext-enable xdebug \
;fi